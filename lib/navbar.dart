import 'package:bottom_navigation/about.dart';
import 'package:bottom_navigation/colorPick.dart';
import 'package:bottom_navigation/home.dart';
import 'package:bottom_navigation/produk.dart';
import 'package:flutter/material.dart';

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  static String tag = 'home-page';
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [new BerandaPage(), Produk(), About()];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home_work,
            color: Warna.biruMuda,
          ),
          icon: new Icon(
            Icons.home_work,
            color: Colors.grey,
          ),
          title: new Text(
            'Home',
          ),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.post_add,
            color: Warna.biruMuda,
          ),
          icon: new Icon(
            Icons.post_add,
            color: Colors.grey,
          ),
          title: new Text('Tambah Produk'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: Warna.biruMuda,
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          title: new Text('About'),
        ),
      ],
    );
  }
}
