import 'package:bottom_navigation/appBar.dart';
import 'package:bottom_navigation/colorPick.dart';
import 'package:flutter/material.dart';

import 'package:bottom_navigation/ui/entryform.dart';
import 'package:bottom_navigation/helpers/dbhelper.dart';
import 'package:bottom_navigation/models/contact.dart';

import 'package:sqflite/sqflite.dart';
import 'dart:async';

class ViewDaftarProduk extends StatefulWidget {
  @override
  ViewDaftarProdukState createState() => ViewDaftarProdukState();
}

class ViewDaftarProdukState extends State<ViewDaftarProduk> {
  DbHelper dbHelper = DbHelper();
  int count = 0;
  List<Contact> contactList;

  @override
  Widget build(BuildContext context) {
    if (contactList == null) {
      contactList = List<Contact>();
    }

    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      body: createListView(),
    ));
  }

  Future<Contact> navigateToEntryForm(
      BuildContext context, Contact contact) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return EntryForm(contact);
    }));
    return result;
  }

  ListView createListView() {
    TextStyle textStyle = Theme.of(context).textTheme.subhead;
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          child: Column(
            children: [
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: Image.network(
                      this.contactList[index].deskripsi,
                      // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                      fit: BoxFit.cover,
                      width: 100,
                      height: 100,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    height: 100,
                    margin: EdgeInsets.only(left: 8),
                    child: Column(
                      children: [
                        Text(
                          this.contactList[index].username,
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.black87,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(20),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.ac_unit_rounded,
                              color: Colors.lightBlueAccent,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Snow Furniture",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Warna.biruMuda,
                                  backgroundColor: Colors.transparent),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomRight,
                    padding: EdgeInsets.only(left: 20, bottom: 10),
                    height: 100,
                    child: Text(
                      this.contactList[index].name,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
              // Card(
              //   color: Colors.white,
              //   elevation: 2.0,
              //   child: ListTile(
              //     leading: CircleAvatar(
              //       backgroundColor: Colors.red,
              //       child: Icon(Icons.people),
              //     ),
              //     title: Text(
              //       this.contactList[index].name,
              //       style: textStyle,
              //     ),
              //     subtitle: Text(this.contactList[index].username),
              //     trailing: GestureDetector(
              //       child: Icon(Icons.delete),
              //       onTap: () {
              //         showDialog(
              //             context: context,
              //             builder: (_) => AlertDialog(
              //                   title: Text('Accept'),
              //                   content: Text('Yakin ingin Menghapus?'),
              //                   actions: [
              //                     FlatButton(
              //                       onPressed: () {
              //                         Navigator.pop(context);
              //                       },
              //                       child: Text('No'),
              //                     ),
              //                     FlatButton(
              //                       onPressed: () {
              //                         deleteContact(contactList[index]);
              //                         Navigator.pop(context);
              //                       },
              //                       child: Text('Yes'),
              //                     )
              //                   ],
              //                   elevation: 0,
              //                   backgroundColor: Colors.white,
              //                 ),
              //             barrierColor: Colors.black.withOpacity(0.7),
              //             barrierDismissible: false);
              //       },
              //     ),
              //     onTap: () async {
              //       var contact = await navigateToEntryForm(
              //           context, this.contactList[index]);
              //       if (contact != null) editContact(contact);
              //     },
              //   ),
              // ),
              // // card
              // Card(
              //   color: Colors.white,
              //   elevation: 2.0,
              //   child: ListTile(
              //     leading: CircleAvatar(
              //       backgroundColor: Colors.red,
              //       child: Icon(Icons.people),
              //     ),
              //     title: Text(
              //       this.contactList[index].deskripsi,
              //       style: textStyle,
              //     ),
              //     subtitle: Text(this.contactList[index].foto),
              //     trailing: GestureDetector(
              //       child: Icon(Icons.delete),
              //       onTap: () {
              //         showDialog(
              //             context: context,
              //             builder: (_) => AlertDialog(
              //                   title: Text('Accept'),
              //                   content: Text('Yakin ingin Menghapus?'),
              //                   actions: [
              //                     FlatButton(
              //                       onPressed: () {
              //                         Navigator.pop(context);
              //                       },
              //                       child: Text('No'),
              //                     ),
              //                     FlatButton(
              //                       onPressed: () {
              //                         deleteContact(contactList[index]);
              //                         Navigator.pop(context);
              //                       },
              //                       child: Text('Yes'),
              //                     )
              //                   ],
              //                   elevation: 0,
              //                   backgroundColor: Colors.white,
              //                 ),
              //             barrierColor: Colors.black.withOpacity(0.7),
              //             barrierDismissible: false);
              //       },
              //     ),
              //     onTap: () async {
              //       var contact = await navigateToEntryForm(
              //           context, this.contactList[index]);
              //       if (contact != null) editContact(contact);
              //     },
              //   ),
              // ),
              // Container(
              //   alignment: Alignment(-0.98, 0.0),
              //   padding: EdgeInsets.symmetric(horizontal: 16),
              //   margin: EdgeInsets.symmetric(vertical: 20),
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       new Text(
              //         'Your Name',
              //         style:
              //             TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              //       ),
              //       Padding(
              //         padding: EdgeInsets.all(4),
              //       ),
              //       new Text(
              //         'Your Description',
              //         style: TextStyle(fontSize: 13),
              //         textAlign: TextAlign.justify,
              //       )
              //     ],
              //   ),
              // ),
            ],
          ),
        );
      },
    );
  }

  //buat contact
  void addContact(Contact object) async {
    int result = await dbHelper.insert(object);
    if (result > 0) {
      // Fluttertoast.showToast(
      //     msg: "Tambah Data Berhasil",
      //     toastLength: Toast.LENGTH_SHORT,
      //     gravity: ToastGravity.CENTER,
      //     timeInSecForIosWeb: 1,
      //     backgroundColor: ColorPallete.primaryColor,
      //     textColor: Colors.white,
      //     fontSize: 16.0);
      updateListView();
    }
  }

  //edit contact
  void editContact(Contact object) async {
    int result = await dbHelper.update(object);
    if (result > 0) {
      updateListView();
    }
  }

  //delete contact
  void deleteContact(Contact object) async {
    int result = await dbHelper.delete(object.id);
    if (result > 0) {
      // Fluttertoast.showToast(
      //     msg: "Delete Data Berhasil",
      //     toastLength: Toast.LENGTH_SHORT,
      //     gravity: ToastGravity.CENTER,
      //     timeInSecForIosWeb: 1,
      //     backgroundColor: ColorPallete.primaryColor,
      //     textColor: Colors.white,
      //     fontSize: 16.0);
      updateListView();
    }
  }

  //update contact
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<Contact>> contactListFuture = dbHelper.getContactList();
      contactListFuture.then((contactList) {
        setState(() {
          this.contactList = contactList;
          this.count = contactList.length;
        });
      });
    });
  }
}
