import 'package:bottom_navigation/appBar.dart';
import 'package:bottom_navigation/colorPick.dart';
import 'package:flutter/material.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _viewList()),
    );
  }

  Widget _viewList() {
    return ListView(
      children: [
        _produk("Nara 3-Seat Sofa", "Rp. 12000000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Nara_3_seat_540x.jpg?v=1580272503"),
        _produk("Newbury Chest", "Rp. 15500000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Newbury_Chest_4_Drawers_ac359dbd-76f7-48c5-8c15-7d968c69e18b_720x.jpg?v=1569092316"),
        _produk("Nara L-Seat Sofa", "Rp. 12000000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Nara_L-Shape_2_seat_720x.jpg?v=1569071784"),
        _produk("Vinoti Living", "Rp. 12000000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Nara_3_seat_540x.jpg?v=1580272503"),
      ],
    );
  }

  Widget _produk(String nama, String harga, String url) {
    return new Container(
        color: Warna.grey,
        child: Card(
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.network(
                  url,
                  // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                  fit: BoxFit.cover,
                  width: 100,
                  height: 100,
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                height: 100,
                margin: EdgeInsets.only(left: 8),
                child: Column(
                  children: [
                    Text(
                      nama,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black87,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.ac_unit_rounded,
                          color: Colors.lightBlueAccent,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Snow Furniture",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Warna.biruMuda,
                              backgroundColor: Colors.transparent),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(left: 20, bottom: 10),
                height: 100,
                child: Text(
                  harga,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ));
  }
}
